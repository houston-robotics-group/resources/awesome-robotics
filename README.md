# Awesome Robotics

## Getting started

This wiki contains all (most) resources we have encountered at the Houston Robotics Group and that we have found to be useful in our paths towards learning the awesome art of making robots!

If you are getting started with learning a new topic you have never dove into before, you will encounter issues with what I would call blank page block. Meaning, you will encounter difficulty knowing where to even get started. There are things that you don't know, and there are things that you don't know that you don't know. These are the most difficult to overcome, since you are unaware of them.

This wiki has the purpose of helping you get started on your robotics journey by giving you a central place to find many useful and high quality resources to learn the art, and giving you access to terms, topics, and other resources that will help you become a better coder, builder, creator, whatever you want to be as a roboticist!

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
